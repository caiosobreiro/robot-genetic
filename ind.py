import random

class Ind():

    path = [] # [(0, 5)] = up for 5 units
    grid = None
    maxLen = None

    def __init__ (self, length, grid):
        self.grid = grid
        self.maxLen = grid.size / 10
        path = []
        while len(path) < length:
            path.append((random.randint(0, 3), random.randint(0, self.maxLen)))
        self.path = self.appendMovesToGetToFinish(path)


    def getFitness (self):
        fitness = 0
        for vect in self.path:
            fitness += vect[1]

        if self.grid.detectOutOfBounds(self.path):
            fitness += 500

        for _ in range(self.grid.detectCollisions(self.path)):
            fitness += 500

        return 1 / fitness


    def __movesToGetToFinish(self, path):

        finalPos = self.grid.getFinalPosition(path)
        moves = []

        remainingX = self.grid.finish[0] - finalPos[0]
        moves.append((1 if remainingX > 0 else 3, remainingX if remainingX > 0 else -remainingX))

        remainingY = self.grid.finish[1] - finalPos[1]
        moves.append((2 if remainingY > 0 else 0, remainingY if remainingY > 0 else -remainingY))

        return moves


    def appendMovesToGetToFinish(self, path):
        newPath = path[:-2]
        # print('newPath', newPath)
        remainingMoves = self.__movesToGetToFinish(newPath)
        # print('moves', remainingMoves, 'newFinalPos:', self.grid.getFinalPosition(newPath + remainingMoves), 'finish:', self.grid.finish)
        return newPath + remainingMoves


    def mutate (self):
        newPath = self.path
        newPath[random.randint(0, len(self.path) - 1)] = (random.randint(0, 3), random.randint(0, self.maxLen))
        self.path = self.appendMovesToGetToFinish(newPath)


    def crossover(self, ind2):
        sublistSelf = self.path[:len(self.path)//2] # first half of self.path
        sublistInd = ind2.path[len(ind2.path)//2:] # second half of ind2
        newPath = sublistSelf + sublistInd
        self.path = self.appendMovesToGetToFinish(newPath)
