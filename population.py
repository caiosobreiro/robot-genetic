from ind import Ind
import random

class Population:

    MUTATION = 0.01
    CROSSOVER = 0.01
    IND_LEN = 10

    pop = []
    size = None
    grid = None

    def __init__(self, size, grid):
        self.size = size
        self.grid = grid
        for i in range(size):
            self.pop.append(Ind(self.IND_LEN, grid))


    def evolve(self):
        newPop = []
        newPop += self.__acceptReject(self.size - 1)

        for ind in newPop:
            # mutation
            if random.random() < self.MUTATION:
                ind.mutate()

            # crossover
            if random.random() < self.CROSSOVER:
                ind.crossover(random.sample(newPop, 1)[0])

        # pass fittest to next gen
        fittest = self.getFittest()
        newPop.append(fittest)
        self.pop = newPop


    def getFittest(self):
        fitnesses = []
        for ind in self.pop:
            fitnesses.append(ind.getFitness())
        fittest = self.pop[fitnesses.index(max(fitnesses))]
        return fittest


    # get n new individuals with fitness-based probability
    def __acceptReject(self, n):
        selected = []

        while len(selected) < n:
            randomInd = random.sample(self.pop, 1)[0]
            r = random.random()
            if randomInd.getFitness() * 40 > r:
                newInd = Ind(self.IND_LEN, self.grid)
                newInd.path = randomInd.path[:]
                selected.append(newInd)
        return selected
