import sys
from grid import Grid
from population import Population


GRID_SIZE = int(sys.argv[1]) if len(sys.argv) > 1 else 50
grid = Grid(GRID_SIZE)

PRINT_GRID = False
if GRID_SIZE < 100:
    PRINT_GRID = True

PRINT = 10
POP_SIZE = 100

if PRINT_GRID:
    grid.print()

genCount = 0
bestFit = 0

# generate initial population
pop = Population(POP_SIZE, grid)

while True:

    pop.evolve()

    if genCount % PRINT == 0:
        fittest = pop.getFittest()
        if PRINT_GRID:
            grid.print(fittest.path)
        print('gen:', genCount, 'fitness:', fittest.getFitness())

    genCount += 1

# print('-----------------------')
# print('Gens:', genCount)
# print('Size:', len(bestInd.path))
# print('ideal length:', grid.startFinishDist)
# print('Mutation rate:', MUTATION)
# print('path found =>', bestInd.path)
# print('-----------------------')
