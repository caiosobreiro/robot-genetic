import random

class Grid:

    grid = []
    size = None
    obstacles = None

    start = ()
    finish = ()

    startFinishDist = None

    directions = {
        'up': 0,
        'right': 1,
        'down': 2,
        'left': 3
    }


    def __init__ (self, size):

        self.size = size
        self.obstacles = size ** 2 * 0.01 # set obstacles to 1% of the area

        self.start = (random.randint(0, self.size - 1), random.randint(0, self.size - 1))
        self.finish = (random.randint(0, self.size - 1), random.randint(0, self.size - 1))

        self.startFinishDist = self.distanceToFinish(self.start)

        for i in range (self.size):
            line = []
            for j in range (self.size):
                line.append(0)
            self.grid.append(line)

        # set start and finish as 2 and 3
        self.grid[self.start[1]][self.start[0]] = 2
        self.grid[self.finish[1]][self.finish[0]] = 3

        # set obstacles randomly on the remaining spaces (where value = 0)
        obstacleCounter = 0
        while obstacleCounter < self.obstacles:
            x, y = (random.randint(0, self.size - 1), random.randint(0, self.size - 1))
            if (self.grid[x][y] == 0):
                self.grid[x][y] = 1
                obstacleCounter += 1


    def __move(self, currentPos, direction):
        if direction == self.directions['up']:
            return (currentPos[0], currentPos[1] - 1)
        elif direction == self.directions['right']:
            return (currentPos[0] + 1, currentPos[1])
        elif direction == self.directions['left']:
            return (currentPos[0] - 1, currentPos[1])
        else: # DOWN
            return (currentPos[0], currentPos[1] + 1)


    def getPositions(self, path):
        positions = []
        currentPos = self.start
        for vect in path:
            for i in range(vect[1]):
                currentPos = self.__move(currentPos, vect[0])
                positions.append(currentPos)
        return positions


    def detectCollisions(self, path):
        positions = self.getPositions(path)
        collisionCount = 0

        for pos in positions:
            if pos[0] < 0 or pos[0] >= self.size or pos[1] >= self.size or pos[1] < 0:
                pass
            elif self.grid[pos[1]][pos[0]] == 1:
                collisionCount += 1

        return collisionCount


    def detectOutOfBounds(self, path):
        positions = self.getPositions(path)
        for pos in positions:
            if pos[0] < 0 or pos[0] >= self.size or pos[1] >= self.size or pos[1] < 0:
                return True
        return False


    def getFinalPosition(self, path):
        return self.getPositions(path)[-1]


    def distanceToFinish (self, currentPos):
        return abs(self.finish[0] - currentPos[0]) + abs(self.finish[1] - currentPos[1])


    def print (self, path = []):

        grid = list(map(list, self.grid)) # "deep clone" grid

        dictionary = {
            0: '\U00002B1B', # open path
            1: '\U00002B1C', # obstacle
            2: '\U0001F535', # start
            3: '\U0000274C', # end
            4: '\U0001F536'  # path
        }

        print ('\n--------------------')
        print (dictionary[0], ' Open space')
        print (dictionary[1], ' Obstacle')
        print (dictionary[2], ' Start')
        print (dictionary[3], ' Finish')
        print (dictionary[4], ' Path')
        print ('--------------------\n')

        positions = self.getPositions(path)[:-1]

        for position in positions:
            if position[1] < self.size and position[1] >= 0 and position[0] < self.size and position[0] >= 0 :
                grid[position[1]][position[0]] = 4

        for i in range (self.size):
            row = ''
            for j in range (self.size):
                row += dictionary[grid[i][j]]
            print (row)
        print ()
